import {state, setState} from "./state";

const SerialPort = require( 'serialport' );
const Readline = require( '@serialport/parser-readline' );




export let serialPort = null;
let serialConnectInterval = null;
let serialReceivedTimeout = null;

const serialConnect = () => {
    SerialPort.list()
        .then( ( list ) => {
            const port = list.filter( ( port ) => port.path.includes( process.env.SERIAL_PORT ) ).pop();
            if ( port ) {
                console.log( 'serial port detected: ', port.path );

                try {
                    serialPort = new SerialPort( port.path, {baudRate: 115200} );

                    const parser = serialPort.pipe( new Readline() );
                    parser.on( 'data', ( data ) => {
                            clearTimeout( serialReceivedTimeout );
                            serialReceivedTimeout = setTimeout( serialTimeout, 5000 );

                            try {
                                const diff = JSON.parse( data );

                                if ( diff.lightSetpoint1 !== state.lightSetpoint1
                                    || diff.lightSetpoint2 !== state.lightSetpoint2
                                    || diff.lightSetpoint3 !== state.lightSetpoint3
                                    || diff.lightSetpoint4 !== state.lightSetpoint4 ) {
                                    serialPort.write( JSON.stringify( {
                                        l1: state.lightSetpoint1,
                                        l2: state.lightSetpoint2,
                                        l3: state.lightSetpoint3,
                                        l4: state.lightSetpoint4
                                    } ), function ( err ) {
                                        if ( err ) {
                                            return console.log( 'Serial port error on write: ', err.message )
                                        }
                                    } )
                                }
                                diff.lightSetpoint1Arduino = diff.lightSetpoint1;
                                diff.lightSetpoint2Arduino = diff.lightSetpoint2;
                                diff.lightSetpoint3Arduino = diff.lightSetpoint3;
                                diff.lightSetpoint4Arduino = diff.lightSetpoint4;
                                delete diff.lightSetpoint1;
                                delete diff.lightSetpoint2;
                                delete diff.lightSetpoint3;
                                delete diff.lightSetpoint4;

                                setState( diff );
                            } catch
                                ( err ) {

                                console.error( 'asdf', data, err );
                            }


                        }
                    );

                    serialWriteState( state, state );


                    console.log( 'serial port connected: ', port.path );
                    setState( {serial: 'connected'} );
                    clearInterval( serialConnectInterval );
                    serialConnectInterval = null;

                    clearTimeout( serialReceivedTimeout );
                    serialReceivedTimeout = setTimeout( serialTimeout, 5000 );
                } catch ( err ) {
                    console.error( err );
                }
            }
        } );
};

export const serialWriteState = ( state, diff ) => {
    if ( serialPort === null ) return;

    const keysToWrite = [
        'lightSetpoint1',
        'lightOn1',
        'lightSetpoint2',
        'lightOn2',
        'lightSetpoint3',
        'lightOn3',
        'lightSetpoint4',
        'lightOn4'
    ];

    const diffShouldBeWritenToSerial = Object.keys( diff ).reduce( ( acc, c ) => acc || ( keysToWrite.includes( c ) && diff[ c ] !== state[ c ] ), false );

    const diffArduinoFromState = (
        'lightSetpoint1Arduino' in diff &&
        'lightSetpoint2Arduino' in diff &&
        'lightSetpoint3Arduino' in diff &&
        'lightSetpoint4Arduino' in diff
    ) && (
        diff.lightSetpoint1Arduino !== ( state.lightOn1 == 0 ? 0 : state.lightSetpoint1 ) ||
        diff.lightSetpoint2Arduino !== ( state.lightOn2 == 0 ? 0 : state.lightSetpoint2 ) ||
        diff.lightSetpoint3Arduino !== ( state.lightOn3 == 0 ? 0 : state.lightSetpoint3 ) ||
        diff.lightSetpoint4Arduino !== ( state.lightOn4 == 0 ? 0 : state.lightSetpoint4 )
    );


    if ( diffShouldBeWritenToSerial || diffArduinoFromState ) {
        const tmpState = Object.assign( {}, state, diff );

        const message = [
            ( tmpState.lightOn1 == 0 ? 0 : tmpState.lightSetpoint1 ),
            ( tmpState.lightOn2 == 0 ? 0 : tmpState.lightSetpoint2 ),
            ( tmpState.lightOn3 == 0 ? 0 : tmpState.lightSetpoint3 ),
            ( tmpState.lightOn4 == 0 ? 0 : tmpState.lightSetpoint4 )
        ].join( ',' );

        serialPort.write( message, function ( err ) {
                if ( err ) {
                    return console.log( 'Serial port error on write: ', err.message )
                }
            }
        )
    }
}

export const serialInit = () => {
    console.log( 'serial init' );
    if ( serialPort === null ) {
        serialConnect();
    }

    serialConnectInterval = setInterval( () => {
        if ( state.serial !== 'connected' ) {
            if ( serialPort !== null ) serialPort.close();
            setState( {serial: 'not-connected'} );
            console.log( 'serial port try connection' );
            serialConnect();
        }

    }, 5000 );
};

const serialTimeout = () => {
    console.log( 'serial port timeout!', 'closing port' );

    setState( {serial: 'timeout'} );

    serialInit();
}