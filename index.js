import {serialInit, serialWriteState} from "./serial";
import {rabbitHandleStateChange, rabbitInit} from "./rabbit";
import {expressInit} from "./express";
import {state} from "./state";


require('dotenv').config()


expressInit();
serialInit();
rabbitInit();

