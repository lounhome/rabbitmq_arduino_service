import {setState, state} from "./state";
import {serialWriteState} from "./serial";

const amqplib = require( 'amqplib' );

const channelName = 'lounhoume-corridor-service';
let rabbitConnection = null;
let channelConnection = null;

const rabbitTopicsToStateKeys = {
    'LounHallLight.setBrightness':     ['lightSetpoint3', 'lightSetpoint4'],
    'LounHallLight.setOn':             ['lightOn3', 'lightOn4'],
    'LounEntranceLight.setBrightness': ['lightSetpoint1', 'lightSetpoint2'],
    'LounEntranceLight.setOn':         ['lightOn1', 'lightOn2'],
};

const stateKeysToRabbitTopics = {
    'lightSetpoint1': 'LounEntranceLight.getBrightness',
    'lightOn1':       'LounEntranceLight.getOn',
    'lightSetpoint2': 'LounEntranceLight.getBrightness',
    'lightOn2':       'LounEntranceLight.getOn',
    'lightSetpoint3': 'LounHallLight.getBrightness',
    'lightOn3':       'LounHallLight.getOn',
    'lightSetpoint4': 'LounHallLight.getBrightness',
    'lightOn4':       'LounHallLight.getOn',

    // 'movementSensor1': 'LounEntranceSensor.getMotionDetected',
    'lightSensor1': 'LounEntranceSensor.getCurrentAmbientLightLevel',
    // 'movementSensor2': 'LounHallSensor.getMotionDetected',
    'lightSensor2': 'LounHallSensor.getCurrentAmbientLightLevel',

    'switch': 'LounHallContactSensor.getContactSensorState'
};

export const rabbitInit = () => {
    // if(rabbitConnection === null){
    rabbitConnection = amqplib.connect( `amqp://${process.env.RABBIT_USERNAME}:${process.env.RABBIT_PASSWORD}@${process.env.RABBIT_SERVER}` );
    // }

    rabbitConnection.then( ( conn ) => {
        if ( channelConnection === null ) {
            return conn.createChannel();
        }
    } ).then( ( ch ) => {
        return ch.assertQueue( channelConnection ).then( function ( ok ) {
            channelConnection = ch;
            rabbitHandleStateChange( {}, state );

            Object.keys( rabbitTopicsToStateKeys ).forEach( ( key ) => {
                ch.bindQueue( channelName, "amq.topic", key );
            } );

            ch.consume( channelName, function ( msg ) {
                if ( msg !== null ) {
                    ch.ack( msg );

                    const value = parseInt( msg.content.toString() );

                    if ( !rabbitTopicsToStateKeys[ msg.fields.routingKey ] ) return;

                    const stateKey = rabbitTopicsToStateKeys[ msg.fields.routingKey ];

                    let diff = {};
                    stateKey.forEach( ( key ) => {
                        if ( key.startsWith( 'lightSetpoint' ) ) {
                            diff[ key ] = Math.round( value / 5 );
                        } else {
                            if ( key.startsWith( 'lightOn' ) ) {
                                const lightNo = key.replace( 'lightOn', '' );
                                if ( value === 1 && state[ 'lightSetpoint' + lightNo ] === 0 ) {
                                    diff[ 'lightSetpoint' + lightNo ] = 12;
                                }
                            }
                            diff[ key ] = value;
                        }

                    } );

                    setState( diff );
                }
            } );
        } );
    } ).catch( ( e ) => {
        console.log( 'channel catch', e );
        // rabbitConnection = null;
        // channelConnection = null;
    } );

    rabbitConnection.catch( ( e ) => {
        console.log( 'connection catch', e );
        // rabbitConnection = null;
        // channelConnection = null;
    } )
};

export const rabbitHandleStateChange = ( state, diff ) => {
    // console.log('tu som', diff);
    if ( rabbitConnection === null || channelConnection === null ) {
        // rabbitInit();

        return;
    }


    Object.keys( diff ).forEach( function ( stateKey ) {
        // console.log(stateKey, stateKeysToRabbitTopics[ stateKey ], state[ stateKey ] !== diff[ stateKey ]);
        if ( stateKeysToRabbitTopics[ stateKey ] && state[ stateKey ] !== diff[ stateKey ] ) {
            // console.log( 'amq.topic',
            //     stateKeysToRabbitTopics[ stateKey ],
            //     ( stateKey.startsWith( 'lightSetpoint' ) ? diff[ stateKey ] * 5 : diff[ stateKey ] ).toString()
            // );
            channelConnection.publish(
                'amq.topic',
                stateKeysToRabbitTopics[ stateKey ],
                Buffer.from( ( stateKey.startsWith( 'lightSetpoint' ) ? diff[ stateKey ] * 5 : diff[ stateKey ] ).toString() )
            );
        }
    } );
};