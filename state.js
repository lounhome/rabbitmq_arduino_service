import {serialWriteState} from "./serial";
import {rabbitHandleStateChange} from "./rabbit";

export let state = {
    serial:         'not-connected',
    lightSetpoint1: 0,
    lightOn1:       0,
    lightSetpoint2: 0,
    lightOn2:       0,
    lightSetpoint3: 0,
    lightOn3:       0,
    lightSetpoint4: 0,
    lightOn4:       0
};


export const setState = ( diff ) => {
    Object.keys( diff ).forEach( ( key ) => {
        if ( key !== 'serial' ) {
            diff[ key ] = parseInt( diff[ key ] );
            diff[ key ] = Math.max( 0, diff[ key ] );
        }

        if ( key.startsWith( 'lightSetpoint' ) ) diff[ key ] = Math.min( diff[ key ], 20 );

        if ( key === 'switch' ) {
            if(state[key] !== undefined && state[key] !== diff[key]){
                diff["lightOn1"] = (state["lightOn1"] > 0) ? 0 : 1;
                diff["lightSetpoint1"] = (diff["lightOn1"] > 0) ? 12 : 0;

                diff["lightOn2"] = (state["lightOn2"] > 0) ? 0 : 1;
                diff["lightSetpoint2"] = (diff["lightOn2"] > 0) ? 12 : 0;

                diff["lightOn3"] = (state["lightOn3"] > 0) ? 0 : 1;
                diff["lightSetpoint3"] = (diff["lightOn3"] > 0) ? 12 : 0;

                diff["lightOn4"] = (state["lightOn4"] > 0) ? 0 : 1;
                diff["lightSetpoint4"] = (diff["lightOn4"] > 0) ? 12 : 0;
            }
        }
    } );

    serialWriteState( state, diff );
    rabbitHandleStateChange( state, diff );

    state = Object.assign( {}, state, diff );
};