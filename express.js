import {setState, state} from "./state";

const express = require( 'express' );

let expressApp = null;

export const expressInit = () => {
    expressApp = express();

    expressApp.use( express.static( 'public' ) );
    expressApp.get( '/status', ( req, res ) => {
        res.send( JSON.stringify( state ) )
    } );

    expressApp.post( '/status', ( req, res ) => {
        setState(req.query);
        res.send( 'Got a POST request');
    } );

    expressApp.listen( 3333, () => console.log( `Example app listening on port 3333!` ) );
};

const serialTimeout = () => {
    console.log( 'serial port timeout!', 'closing port' );

    setState( {serial: 'timeout'} );

    serialInit();
}